package com.example.musicbrainz.ui.gallery;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class collectionViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public collectionViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Halaman untuk melihat koleksi pribadi");
    }

    public LiveData<String> getText() {
        return mText;
    }
}