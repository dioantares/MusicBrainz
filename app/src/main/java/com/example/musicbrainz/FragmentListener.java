package com.example.musicbrainz;

public interface FragmentListener {
    void changePage(int page);

    void closeApplication();
}
