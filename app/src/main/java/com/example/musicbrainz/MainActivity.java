package com.example.musicbrainz;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.Button;
import android.widget.EditText;

import com.example.musicbrainz.ui.home.HomeViewModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private AppBarConfiguration mAppBarConfiguration;
    private NetworkInfo networkInfo;
    private EditText etEkspresi;
    private Button btnHitung;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Belum ada action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        this.etEkspresi = findViewById(R.id.et_ekspresi);
        this.btnHitung = findViewById(R.id.btn_hitung);
        this.btnHitung.setOnClickListener(this);
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        this.networkInfo = connMgr.getActiveNetworkInfo();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public void onClick(View v) {
        if (v == this.btnHitung) {
            String input = this.etEkspresi.getText().toString();
            if (this.networkInfo != null && this.networkInfo.isConnected()) {
                new SearchAsyncTask(this).execute(input);
            }
        }
    }
}

//public class MainActivity extends AppCompatActivity implements FragmentListener {
//    private NetworkInfo networkInfo;
//    private FragmentManager fragmentManager;
//    private MainFragment mainFragment;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
//        this.mainFragment = new MainFragment();
//        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        this.networkInfo = connMgr.getActiveNetworkInfo();
//        FragmentTransaction ft = this.fragmentManager.beginTransaction();
//        ft.add(R.id.fragment_container, this.mainFragment).addToBackStack(null).commit();
//    }
//
//    @Override
//    public void changePage(int page) {
//        FragmentTransaction ft = this.fragmentManager.beginTransaction();
//        if (page == 1) {
//            if (this.mainFragment.isAdded()) {
//                ft.show(this.mainFragment);
//            } else {
//                ft.add(R.id.fragment_container, this.mainFragment);
//            }
//            if (this.mainFragment.isAdded()) {
////                ft.hide(this.mainFragment);
//            }
//        }
//    }

//    @Override
//    public void closeApplication() {
//        this.moveTaskToBack(true);
//        this.finish();
//    }
//}