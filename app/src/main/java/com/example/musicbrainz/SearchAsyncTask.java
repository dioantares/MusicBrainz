package com.example.musicbrainz;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class SearchAsyncTask extends AsyncTask<String, String, String> {
    private final String BASE_URL = "https://musicbrainz.org/ws/2/artist?query=artist:";
    private final String expr = "expr";
    private MainActivity mainActivity;

    public SearchAsyncTask(MainActivity mainActivity){
        this.mainActivity = mainActivity;
    }

    @Override
    protected String doInBackground(String... strings) {
        String hasil = "";
        Uri buildURI = Uri.parse(BASE_URL).buildUpon()
                .appendQueryParameter(expr, strings[0])
                .build();
        URL requestURL = null;
        HttpURLConnection conn = null;
        InputStream is = null;
        try {
            requestURL = new URL(buildURI.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            conn = (HttpURLConnection) requestURL.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.connect();
            int response = conn.getResponseCode();
            if (response == 200) {
                is = conn.getInputStream();
                hasil = convertIsToString(is);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.disconnect();
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return hasil;
    }

    protected void onPostExecute(String s) {
        Log.d("Hasil", s);
//        this.mainActivity.setText(s);
    }

    public String convertIsToString(InputStream stream) throws IOException {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = reader.readLine()) != null) {
            builder.append(line).append("\n");
        }
        if (builder.length() == 0) {
            return null;
        }
        return builder.toString();
    }
}
